# Eco Server Docker

this is a docker compose setup which uses the [StrangeloopGames Docker image](https://hub.docker.com/r/strangeloopgames/eco-game-server) to start a Eco Server inside a docker container.

The Config files are adjusted to be perfect for one player.

the main branch uses [Version 0.10.2.4-beta](https://hub.docker.com/r/strangeloopgames/eco-game-server/tags?page=&page_size=&ordering=&name=0.10.2.4-beta) you can create a branch update the docker compose and use the newer docker images. Make sure to add the correct default config files in mount/Configs.

## Requirements
- [Docker](https://docs.docker.com/engine/install/) is installed
    - [Docker Compose](https://docs.docker.com/compose/install/) plugin is installed (2024 compose is default on ubuntu docker installation)

## Usage
- clone this repo : `git clone https://gitlab.com/Oskar1504/eco-server-docker`
- start server `docker compose up -d`

### how to connect
- start Eco on your pc
- go to "Your Worlds" in main menu
- click the plus icon in the top right
- enter your server ip addres with the port (default :3000)
    - example: 123.123.2.123:3000


## Configuration
Theres a raw overview [on the official wiki](https://wiki.play.eco/en/Server_Configuration)

### important config files
- [Difficulty.eco](mount/Configs/Difficulty.eco) configures most changed values like stack size, stockpile range, growTime
- [Network.eco](mount/Configs/Network.eco) configures stuff like server description, server password and other important server owner stuff


## I want to play on another version
- Fork this repository
- [find your docker version tag](https://hub.docker.com/r/strangeloopgames/eco-game-server/tags?page=&page_size=&ordering=&name=)
- create branch with version tag you will use from the docker image
- adjust the [docker-compose.yml](docker-compose.yml) on line 4 `image: strangeloopgames/eco-game-server:<YOUR_IMAGE_VERSION_TAG>`
- make sure to copy the right Config files into the [mount/Configs](mount/Configs/) directory

### How do i get default config files

### Steam on your pc
- Start Eco with the correct version via Steam
- Start "singleplayer"/lokal server/world
- go to `SteamLibrary\steamapps\common\Eco\Eco_Data\Server\Configs`
    - this is the location of all you steam games this varies based on you installation path
- copy Config files


#### Online server download
- [Login into your Eco Account](https://play.eco/account)
- download server.zip
- extract .zip
- start EcoServer.exe
- Close it after configuriation via the GUI
- Copy Config files